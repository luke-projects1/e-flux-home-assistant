## Authentication

POST
https://api.e-flux.nl/1/auth/login

body:
```json
{
    {
        "email":"_EMAIL_",
        "password":"_PASSWORD_"}
}
```

<hr>

## Invoices

POST
https://api.e-flux.nl/1/invoices/mine

authentication: Bearer

body:
```json
{
	"limit":20,
    "skip":0,
	"sort":
		{
			"order":"desc",
			"field":"createdAt"
		}
}
```

<hr>

## Sessions

POST
https://api.e-flux.nl/2/sessions/cpo/mine

authentication: Bearer